//
//  MainViewController.swift
//  SoundBoard
//
//  Created by Abdullah Al-Ahmadi on 1/30/17.
//  Copyright © 2017 Abdullah Al-Ahmadi. All rights reserved.
//

import UIKit
import AVFoundation


class MainViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
  
  @IBOutlet weak var audioList: UITableView!
  
  var soundArray : [Sound] = []
  var audioPlayer : AVAudioPlayer?
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    audioList.dataSource = self
    audioList.delegate = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    do {
      try soundArray = context.fetch(Sound.fetchRequest())
      audioList.reloadData()
    } catch {
      
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return soundArray.count
    
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell()
    let selectedAudio = soundArray[indexPath.row]
    cell.textLabel?.text = selectedAudio.name
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let selectedAudio = soundArray[indexPath.row]
    do {
      try audioPlayer = AVAudioPlayer(data: selectedAudio.audio as! Data)
      audioPlayer?.play()
    } catch {}
    
    tableView.deselectRow(at: indexPath, animated: true)
  }
  
  // Swipe to delete audio from the list
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      let selectedAudio = soundArray[indexPath.row]
      let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
      context.delete(selectedAudio)
      (UIApplication.shared.delegate as! AppDelegate).saveContext()
      do {
        try soundArray = context.fetch(Sound.fetchRequest())
        audioList.reloadData()
      } catch {
        
      }
      
    }
    
  }
  
}




