//
//  ViewController.swift
//  SoundBoard
//
//  Created by Abdullah Al-Ahmadi on 1/30/17.
//  Copyright © 2017 Abdullah Al-Ahmadi. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
  
  @IBOutlet weak var recordButton: UIButton!
  @IBOutlet weak var playButton: UIButton!
  @IBOutlet weak var addButton: UIButton!
  @IBOutlet weak var audioLabel: UITextField!
  
  var audioRecorder : AVAudioRecorder?
  var audioPlayer : AVAudioPlayer?
  var audioURL : URL?
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupRecorder()
    
  }
  
  func setupRecorder() {
    
    do {
      // Create an audio session
      
      let session = AVAudioSession.sharedInstance()
      try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
      try session.overrideOutputAudioPort(.speaker)
      try session.setActive(true)
      
      // Create an URL for thee audio
      
      let basePath : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
      let pathComponent = [basePath, "audio.m4a"]
      audioURL = NSURL.fileURL(withPathComponents: pathComponent)!
      print("#######################")
      print(audioURL!)
      print("#######################")
      
      
      // Create settings for the audio recorder
      
      var settings : [String:AnyObject] = [:]
      settings[AVFormatIDKey] = Int(kAudioFormatMPEG4AAC) as AnyObject?
      settings[AVSampleRateKey] = 44100.0 as AnyObject?
      settings[AVNumberOfChannelsKey] = 2 as AnyObject?
      
      
      // Create audio record object
      
      try audioRecorder = AVAudioRecorder(url: audioURL!, settings: settings)
      audioRecorder?.prepareToRecord()
      
      
    } catch let error as NSError {
      print (error)
    }
    
  }
  
  @IBAction func recordButtonTapped(_ sender: Any) {
    print("record button tapped")
    
    if audioRecorder!.isRecording {
      // stop recording
      audioRecorder?.stop()
      // change stop button to record
      recordButton.setTitle("Record", for: .normal)
      // enable play button
      playButton.isEnabled = true
      // enable add button
      addButton.isEnabled = true
    } else {
      // start recording
      audioRecorder?.record()
      // change record button to stop
      recordButton.setTitle("Stop", for: .normal)
      
    }
  }
  @IBAction func playButtonTapped(_ sender: Any) {
    print("Play button tapped")
    do {
      try audioPlayer = AVAudioPlayer(contentsOf: audioURL!)
      audioPlayer?.play()
    } catch  {
      
    }
  }
  
  @IBAction func addButtonTapped(_ sender: Any) {
    print("add button tapped")
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let sound = Sound(context: context)
    sound.name = audioLabel.text
    sound.audio = NSData(contentsOf: audioURL!)
    (UIApplication.shared.delegate as! AppDelegate).saveContext()
    
    //navigationController?.popViewController(animated: true)
    
    
    
  }
  
  
}

